#!/usr/local/bin/ruby

$ROOTDIR = File.expand_path(File.dirname(__FILE__))
$LOAD_PATH.unshift "#{$ROOTDIR}/"
$LOAD_PATH.unshift "#{$ROOTDIR}/libraries/"
$LOAD_PATH.unshift "#{$ROOTDIR}/extensions/"
require 'chariot'
require "libkaiser"
$chariot = Chariot.new
$chariot.refresh
$chariot.loadExtensions
$chariot.begin
=begin
  
rescue Exception => e
  $chariot.say e.message
  for bt in e.backtrace
    $chariot.output "-- #{bt}\n"
  end
=end
