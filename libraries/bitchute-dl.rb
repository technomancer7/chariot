require 'faraday'
require 'nokogiri'

class BitchuteDL
  def initialize(url)
    res = Faraday.get url
    doc = Nokogiri::HTML res.body
    doc.css('source').each do |link|
      @stream = link["src"]
    end
    doc.css('h1', 'page-title').each do |link|
      @title = link.content
    end
    doc.css('p').each do |link|
      if link.classes.include? "owner"
        @owner = "#{link.content.strip}"
        @owner_url = "https://www.bitchute.com#{link.css("a")[0]["href"]}"
      end
      if link.classes.include? "name"
        @name = "#{link.content.strip}"
        @name_url = "https://www.bitchute.com#{link.css("a")[0]["href"]}"
      end
    end
    doc.css('div').each do |link|
      if link.classes.include? "full" and link.classes.include? "hidden"
        @description = "#{link.content.strip}"
      end
    end
    doc.css('table').each do |link|
      if link.classes.include? "video-detail-list"
        td = link.css('td')
        @category = "#{td[1].content}"
        @sensitivity = "#{td[3].content}"
      end
    end
  end

  attr_accessor :name
  attr_accessor :name_url
  attr_accessor :owner
  attr_accessor :owner_url
  attr_accessor :description
  attr_accessor :stream
  attr_accessor :category
  attr_accessor :sensitivity
  
  def BitchuteDL.from_search(term)
    # do search
    #return BitchuteDL.new term
  end
end

module Bitchute
  def Bitchute.search(term)
    # Handle searching
    return BitchuteDL.new term # < --- change to url
  end

  def Bitchute.dl(url)
    return BitchuteDL.new url
  end
end
