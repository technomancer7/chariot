require 'faraday'
require 'nokogiri'
module Netcrawler
  $cache = {}
  
  def Netcrawler.searchStartpage(query, domain: "", description: "", title: "")
    if $cache.key? "sp.#{query}"
      return $cache["sp.#{query}"]
    end
    resp = []
    q = query.dup.gsub!(" ", "+")
    res = Faraday.get "https://html.duckduckgo.com/html/?q=#{query}&norw=1&t=ffab&ia=web"
    doc = Nokogiri::HTML res.body
    c = {}
    publish = true
    doc.css("result__snippet result__a result__url", 'a').each do |link|
      if link.classes.include? "result__a"
        if title != "" and not link.content.strip.chomp.downcase.include? title.downcase
          publish = false
        end
        c["title"] = link.content.strip.chomp
      elsif link.classes.include? "result__url"
        if domain != "" and not link.content.strip.chomp.downcase.include? domain.downcase
          publish = false
        end
        c["url"] = "https://#{link.content.strip.chomp}"
      elsif link.classes.include? "result__snippet"
        if description != "" and not link.content.strip.chomp.downcase.include? description.downcase
          publish = false
        end
        c["description"] = link.content.strip.chomp
        resp.append c
        c = {}
        publish = true
      end
      
      
    end

    $cache["sp.#{query}"] = resp
    resp
  end
  def searchDDG(query)
    res = Faraday.get ""
    puts res.body
  end
  def searchQwant(query)
    res = Faraday.get ""
    puts res.body
  end
end
