

def load_fun(c)
  # forgot trying to hook in to dgames for now, hook directly in to the ruby files extensions
  c.brain.addCommand("dggames") { |s, args|
    r = IO.popen("dgames #{args}", "r+")
    c.redirect { |a| 
      r.write "#{a}\n"

      c.output r.gets
      ""
    }
    ""
  }
end

def unload_fun(c)
  c.delCommand("dggames")
end
