require "bitchute-dl"

def load_video(c)
  c.brain.addCommand("bitchute") { |s, args|
    ln = args[0];
    unless ln.start_with? "http"
      # Do bitchute search here...
    else
      say "Playing from URL; #{ln}"
    end

    g = BitchuteDL.new ln
    cmd = getv "tools", "video", "vlc"
    args = getv "tools", "video", ""
    Thread.new {
      system "#{cmd} #{g.stream} #{args}"
    }
    ""
  }
  c.brain.addCommand("stopvideo") { |s, args|
    cmd = getv "tools", "video", "vlc"
    Thread.new {
      system "pkill #{cmd}"
    }
    ""
  }      
end

def unload_video(c)
  c.brain.delCommand("bitchute")
  c.brain.delCommand("stopvideo")
end
