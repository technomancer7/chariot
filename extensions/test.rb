
def load_test(c)
  puts "Test loaded in #{c}"
  c.addExtButton("test", "Test") { |c| puts c}
end

def unload_test(c)
  puts "Test unloaded"
  c.delExtButton("test", "Test")
end
