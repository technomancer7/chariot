def load_audio(c)
  c.addExtButton("audio", "Internet Radio") { |c|
    data = getv "radio", "stations", []
    window = Gtk::Window.new
    window.title = title
    lines = Gtk::VBox.new
    controls = Gtk::HBox.new
    add = Gtk::Button.new "+"
    save = Gtk::Button.new "Save"
    controls.add add;controls.add save;
    window.add lines
    lines.add controls

    @linesc = {}
  }

  c.brain.addCommand("radio") { |s, args|
    ln = args[0];
    unless ln.start_with? "http"
      stations = getv "radio", "stations", []
      stations.each do |k, s|
        if s["name"].downcase.include? ln.downcase
          ln = s["url"]
          say "Playing #{s["name"]}."
          output s["description"]
          break
        end
      end
    else
      say "Playing from URL; #{ln}"
    end
    cmd = getv "tools", "audio", "vlc"
    args = getv "tools", "audio", "-I dummy"
    Thread.new {
      system "#{cmd} #{ln} #{args}"
    }
    ""
  }
  c.brain.addCommand("stopplayer") { |s, args|
    cmd = getv "tools", "audio", "vlc"
    Thread.new {
      system "pkill #{cmd}"
    }
    ""
  }      
end

def unload_audio(c)
  c.delExtButton("audio", "Internet Radio")
  c.brain.delCommand("radio")
  c.brain.delCommand("stopplayer")
end
